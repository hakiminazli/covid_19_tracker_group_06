<?php

   ob_start();
   session_start();
       
    //Define your Server host name here.
    $HostName = "localhost";
    
    //Define your MySQL Database Name here.
    $DatabaseName = "id15491881_covid19trackerdb";
    
    //Define your Database User Name here.
    $HostUser = "id15491881_group06";
    
    //Define your Database Password here.
    $HostPass = "Rafikeya@g0610"; 
    
    
    $con = new mysqli($HostName,$HostUser,$HostPass,$DatabaseName);
    
    if($con){
        
    	 
    }else{
    	echo "Connection Failed";
    	exit();
    }
        
        if($_SERVER['REQUEST_METHOD']=='POST'){

            $adminID= $_POST['adminID'];
            $password = $_POST['password'];
            
            $sql= "SELECT staffName, staffID, staffIC FROM staff_info";
            $result = mysqli_query($con, $sql);
                        
            if(mysqli_num_rows($result)){
                while($row = mysqli_fetch_assoc($result)){
                     if($adminID == $row['staffID'] && $password == $row['staffIC']){
                         
                        $_SESSION['valid'] = true;
                        $_SESSION['timeout'] = time();
                        $_SESSION['adminID'] = $row['staffID'];
                        header("Location: https://covid19trackerdb.000webhostapp.com/Web/homePage.php"); 
                        exit;
                     }else{
                        echo '<script>alert("Wrong Credentials!!! Try Again")</script>'; 
                     } 
                }            
            }else{
                
            }
        }
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.1/assets/img/favicons/favicon.ico">

    <title>Admin Login- Covid-19 Tracker</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <!--<link href="loginAdmin.css" rel="stylesheet">-->
    
    <style>
        html,
        body {
          height: 100%;
          color: cornsilk;
        }
        
        
        body {
          display: -ms-flexbox;
          display: flex;
          -ms-flex-align: center;
          align-items: center;
          padding-top: 40px;
          padding-bottom: 40px;
          /*background-color: #f5f5f5;*/
          background-color: #74EBD5;
            background-image: linear-gradient(90deg, #74EBD5 0%, #9FACE6 100%);
        }
        
        .form-signin {
          width: 100%;
          max-width: 330px;
          /*padding: 15px;*/
          margin: auto;
          background-color: rgba(12, 7, 21, 0.25);
            padding: 24px;
            border-radius: 15px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .form-signin .checkbox {
          font-weight: 400;
        }
        .form-signin .form-control {
          position: relative;
          box-sizing: border-box;
          height: auto;
          padding: 10px;
          font-size: 16px;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="email"] {
          margin-bottom: -1px;
          border-bottom-right-radius: 0;
          border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
    </style>
  </head>

  <body class="text-center">

    <form class="form-signin" action = "<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method = "post">

      <img class="mb-4" src="assets/TRACKER.png" alt="covid-19 tracker" width="72">
      <h1 class="h3 mb-3 font-weight-normal">Please Sign In</h1>

      <label for="inputID" class="sr-only">Admin ID</label>
      <input name="adminID" type="text" id="inputID" class="form-control" placeholder="Admin ID" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>

      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form>

  </body>
</html>