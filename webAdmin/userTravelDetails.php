<?php
    // Start the session
    session_start();
    include 'dbConnection.php';
   
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Travel History</title></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    
    <!-- Our Custom CSS -->
  
    <!-- Font Awesome JS -->
    <script src="https://kit.fontawesome.com/43d73fdaf8.js" crossorigin="anonymous"></script>
    
    
    <style>
        /* Style inputs, select elements and textareas */
        
        .container {
         
            padding: 100px 100px 100px 100px;
            align-items: center;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            position:relative;
            margin-top: 30px;
            color: black;
            background-color: rgba(12, 7, 21, 0.25);
            padding: 30px;
            border-radius: 15px;
            
          }  
        .table-row{
            cursor:pointer;
        }

          /* Style the label to display next to the inputs */
          label {
            padding: 12px 12px 12px 0;
            display: inline-block;
          }
          
          /* Style the submit button */
          input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            float: right;
            margin-left: 15px;
            margin-top: 20px;
            
          }
        
          
          /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
          @media screen and (max-width: 600px) {
            .col-25, .col-75, input[type=submit],input[type=reset]{
              width: 100%;
              margin-top: 2px;
              margin-right: 0px;
            }
          } 
        
        
        
          footer {
            padding-top: 10px;
            padding-bottom: 20px;
            background-color:white;
            position:absolute;
            bottom: 10px;
            height: 2.5rem;  
        }
          
        footer p {
            margin-bottom: .25rem;
        }
        
       .sidebar-header img{
            width: 70px;   
            display:block;
            margin: 30px auto;
            padding: auto;
        
        }
        
                /*
            DEMO STYLE
        */
        @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";
        
        
        body {
            font-family: 'Poppins', sans-serif;
            /*background: #fafafa;*/
            /*background-color: #00DBDE;*/
            /*background-image: linear-gradient(90deg, #00DBDE 0%, #FC00FF 100%);*/
            background-color: #74EBD5;
            background-image: linear-gradient(90deg, #74EBD5 0%, #9FACE6 100%);

        }
        
        /*#myTable thead{*/
        /*    background-color:rgba(12, 7, 21, 0.9);*/
        /*    color: white;*/
        /*}*/
        
        /*#myTable thead:hover{*/
        /*    background-color:rgba(12, 7, 21, 0.9);*/
        /*}*/
        
        /*#myTable{*/
        /*    border: 1px solid rgba(12, 7, 21, 0.9);*/
        /*}*/
        
        td{
            background-color: rgba(12, 7, 21, 0.9);
        }
        
        thead:hover{
         background-color: black;
        }
        
        p {
            font-family: 'Poppins', sans-serif;
            font-size: 1.1em;
            font-weight: 300;
            line-height: 1.7em;
            color: #999;
        }
        
        a, a:hover, a:focus {
            color: inherit;
            text-decoration: none;
            transition: all 0.3s;
        }
        
        .navbar {
            background-color: rgba(12, 7, 21, 0.9);
            padding: 15px 10px;
            border: none;
            border-radius: 1%;
            margin-bottom: 30px;
            box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.19);
        }
        
        .navbar-btn {
            box-shadow: none;
            outline: none !important;
            border: none;
        }
        
        .line {
            width: 100%;
            height: 1px;
            border-bottom: 1px dashed #ddd;
            margin: 40px 0;
        }
        
        .bg-modal{
            width:100%;
            height:100%;
            background-color: rgb(0,0,0,0.7);
            position: absolute;
            top:0;
            display: none;
            justify-content: center;
            align-items: center;
            background-size: cover;
            overflow: hidden;
            
        }
        
        .model-content{
            width: 500px;
            height: 300px;
            background-color: white;
            border-radius: 15px;
            text-align:center;
            padding: 20px;
            position: relative;
            
            
        }
        
        .close{
            position: absolute;
            right: 20px;
            transform: rotate(45deg);
            cursor: pointer;
            font-size: 30px;
        }
        
        .model-content input{
            width:50%;
            display:block;
            margin:15px auto;
        }
        
        .btn-info{
            text-align:center;
        }
        
        /* ---------------------------------------------------
            SIDEBAR STYLE
        ----------------------------------------------------- */
        
        /*.wrapper{*/
        /*    display: flex;*/
        /*    width: 100%;*/
        /*    align-items: stretch;*/
        /*    perspective: 1500px;*/
        /*}*/
        
        
        #sidebar {
            min-width: 250px;
            max-width: 250px;
            background-color: rgba(12, 7, 21, 0.9);
            color: #fff;
            transition: all 0.6s cubic-bezier(0.945, 0.020, 0.270, 0.665);
            transform-origin: bottom left;
            position:fixed;
            height: 100%;
            border-right: 7px solid #00DBDE;
            /*overflow-y: scroll;*/
             
        }
        
        /*#sidebar.active {*/
        /*    margin-left: -250px;*/
        /*    transform: rotateY(100deg);*/
        /*}*/
        
        #sidebar .sidebar-header {
            padding: 20px;
            background: rgba(0,0,1,0);
            border-radius: 15px;
        }
        
        #sidebar ul.components {
            padding: 20px 0;
            border-top: 1px solid #62c1cd;*/
        }
        
        #sidebar ul p {
            color: rgba(0,0,1,0.3);
            padding: 10px;
        }
        
        #sidebar ul li a {
            padding: 10px;
            font-size: 1.1em;
            display: block;
        }
        #sidebar ul li a:hover {
            color: #62c1cd;
            background: #fff;
        }
        
        #sidebar ul li.active > a, a[aria-expanded="true"] {
            color: #fff;
            background: #00DBDE;
        }
        
        
        a[data-toggle="collapse"] {
            position: relative;
        }
        
        .dropdown-toggle::after {
            display: block;
            position: absolute;
            top: 50%;
            right: 20px;
            transform: translateY(-50%);
        }
        
        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: rgb(43, 44, 49);
        }
        
        ul.CTAs {
            padding: 20px;
        }
        
        ul.CTAs a {
            text-align: center;
            font-size: 0.9em !important;
            display: block;
            border-radius: 5px;
            margin-bottom: 5px;
        }
        
        a.logout {
            margin-top:100px;
            background: #00DBDE;
            color: #fff;
        }
        
        a.article, a.article:hover {
            background: rgb(43, 44, 49) !important;
            color: #fff !important;
        }
        
        
        
        /* ---------------------------------------------------
            CONTENT STYLE
        ----------------------------------------------------- */
        #content {
            width: 81%;
            padding: 20px;
            min-height: 100vh;
            transition: all 0.3s;
            margin-left: 19%;
        }
        
        /*#sidebarCollapse {*/
        /*    width: 40px;*/
        /*    height: 40px;*/
        /*    background:rgb(43, 44, 49);*/
        /*    cursor: pointer;*/
        /*}*/
        
        /*#sidebarCollapse span {*/
        /*    width: 80%;*/
        /*    height: 2px;*/
        /*    margin: 0 auto;*/
        /*    display: block;*/
        /*    background: #fff;*/
        /*    transition: all 0.8s cubic-bezier(0.810, -0.330, 0.345, 1.375);*/
        /*    transition-delay: 0.2s;*/
        /*}*/
        
        /*#sidebarCollapse span:first-of-type {*/
        /*    transform: rotate(45deg) translate(2px, 2px);*/
        /*}*/
        /*#sidebarCollapse span:nth-of-type(2) {*/
        /*    opacity: 0;*/
        /*}*/
        /*#sidebarCollapse span:last-of-type {*/
        /*    transform: rotate(-45deg) translate(1px, -1px);*/
        /*}*/
        
        
        /*#sidebarCollapse.active span {*/
        /*    transform: none;*/
        /*    opacity: 1;*/
        /*    margin: 5px auto;*/
        /*}*/
        
        
        /* ---------------------------------------------------
            MEDIAQUERIES
        ----------------------------------------------------- */
        @media (max-width: 899px) {
            #sidebar {
                margin-left: -250px;
                transform: rotateY(90deg);
                position:fixed;
                 z-index: 1;
            }
            
            #content {
                width: 100%;
                /*padding: 20px;*/
                /*min-height: 100vh;*/
                /*transition: all 0.3s;*/
                margin-left: auto;
            }
            /*#sidebar.active {*/
            /*    margin-left: 0;*/
            /*    transform: none;*/
            /*}*/
            /*#sidebarCollapse span:first-of-type,*/
            /*#sidebarCollapse span:nth-of-type(2),*/
            /*#sidebarCollapse span:last-of-type {*/
            /*    transform: none;*/
            /*    opacity: 1;*/
            /*    margin: 5px auto;*/
            /*}*/
            /*#sidebarCollapse.active span {*/
            /*    margin: 0 auto;*/
            /*}*/
            /*#sidebarCollapse.active span:first-of-type {*/
            /*    transform: rotate(45deg) translate(2px, 2px);*/
            /*}*/
            /*#sidebarCollapse.active span:nth-of-type(2) {*/
            /*    opacity: 0;*/
            /*}*/
            /*#sidebarCollapse.active span:last-of-type {*/
            /*    transform: rotate(-45deg) translate(1px, -1px);*/
            /*}*/
        
        }

    </style>


</head>

<body>
 <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <a href='homePage.php'><img src="assets/TRACKER.png" alt="Coivd-19 Tracker"></a>
            </div>

            <ul class="list-unstyled components">
                <li>
                    <a href="homePage.php"><i class="fas fa-tachometer-alt"></i>  Dashboard</a>
               <li> 
                    <a href="uploadNewsPage.php"><i class="fas fa-newspaper"></i>  Upload News</a>
                </li>
                 <li class="active">
                    <a href="travelHistory.php"><i class="fas fa-route"></i>  Travel History</a>
                </li>
                 <li>
                    <a href="redZone.php"><i class="fas fa-head-side-mask"></i>  Red Zones</a>
                </li>
                <!--<li>-->
                <!--    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Actions</a>-->
                <!--    <ul class="collapse list-unstyled" id="pageSubmenu">-->
                <!--        <li class="active">-->
                <!--            <a href="uploadNewsPage.php">Upload News</a>-->
                <!--        </li>-->
                <!--        <li>-->
                <!--            <a href="travelHistory.php">User Travel History</a>-->
                <!--        </li>-->
                <!--        <li>-->
                <!--            <a href="#">Red Flagge Areas</a>-->
                <!--        </li>-->
                <!--    </ul>-->
                <!--</li>-->
                <!--<li>-->
                <!--    <a href="#">Profile</a>-->
                <!--</li>-->
                <li>
                    <a href="help.php"><i class="fas fa-question-circle"></i>  Help</a>
                    
                </li>
                <!--<li>-->
                    <!--<a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>-->
                <!--    <a class="logout" href="logoutAdmin.php"><i class="fas fa-sign-out-alt"></i> Sign Out </a>-->
                <!--</li>-->
            </ul>

            <!--<ul class="list-unstyled CTAs">-->
            <!--    <li>-->
                    <!--<a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>-->
            <!--        <a class="logout" href="logoutAdmin.php">Sign Out <i class="fas fa-sign-out-alt"></i></a>-->
            <!--    </li>-->
            <!--</ul>-->
        </nav>
     
        <!-- Page Content Holder -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-dark bg">
                <div class="container-fluid">

                    <!--<button type="button" id="sidebarCollapse" class="navbar-btn">-->
                    <!--    <span></span>-->
                    <!--    <span></span>-->
                    <!--    <span></span>-->
                    <!--</button>-->
                    <!--<button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">-->
                    <!--    <i class="fas fa-align-justify"></i>-->
                    <!--</button>-->
                    <div class="title">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link"  href="#"><h4 style="margin:0;">Travel History </h4></a>
                            </li>
                        </ul>
                    </div>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="homePage.php">Dashboard</a>
                            </li>
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            Dropdown link
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Link 1</a>
                                <a class="dropdown-item" href="#">Link 2</a>
                                <a class="dropdown-item" href="#">Link 3</a>
                            </div>
                        </li>
                            <li class="nav-item">
                                 <a class="nav-link" data-toggle="tooltip" title="Sign Out"href="logoutAdmin.php"><i class="fas fa-sign-out-alt"></i></a>
                            </li>
                         
                        </ul>
                    </div>
                </div>
            </nav>
        
            <div class="container">
                <?php
                
                    // $IC = $_SESSION['IC'];
                    $IC = $_GET['ic'];
                    $sql= "SELECT userID, IC, name, gender FROM user_info WHERE IC = '$IC'";
                    $result = $con->query($sql);
                    
                    $ID;
                    
                    if($result){            
                        if($result->num_rows == 1){
                            $row = $result->fetch_assoc();
                            $ID = $row['userID'];
                            echo "<h5><b>Name:</b> ".$row['name']."</h5>";
                            echo "<h5><b>IC:</b> ".$row['IC']."</h5>";
                            echo "<h5><b>Gender:</b> ".$row['gender']."</h5>";
                            // echo "<h5><b>State:</b> ".$row['state']."</h5>";
                                                                
                        }else{
                            echo "error";
                        }
                                                
                    }else{
                        echo "error".$sql."<br>".$con->error;
                    }
                    
                    echo "<hr>";
                              
                    echo "<table id='myTable' class='table table-bordered table-dark table-hover'>";
                    echo "<thead>";
                    echo  "<tr>";
                    echo    "<th>City</th>";
                    echo    "<th>District</th>";
                    echo    "<th>State</th>";
                    // echo    "<th>Status</th>";
                    echo    "<th>Date & Time</th>";
                    echo  "</tr>";
                    echo "</thead>";
                    echo "<tbody>";
                
            
                    $sql= "SELECT currCity, curDistrict, curState, timeRecorded, dateRecorded FROM travel_history WHERE userID='$ID'";
                    $result = $con->query($sql);
                
                        
                    if($result->num_rows > 0){
                        while($row = $result->fetch_assoc()){
                               
                            echo "<tr class='table-row'>";
                            echo "<td>".$row["currCity"]."</td>";
                            echo "<td>".$row["curDistrict"]."</td>";
                            echo "<td>".$row["curState"]."</td>";
                            echo "<td>".$row["dateRecorded"]."/".$row["timeRecorded"]."</td>";
                            echo "</tr>";
                               
                        }
                            
                    }else{
                        
                    }
                        
                   
                     $con->close();   
                ?>
                
                </tbody>
                    
                </table> 
        
        
            </div>
      
        </div>
    </div>
    <!-- /#page-content-wrapper -->

    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
  
    <script>
        $(document).ready(function() {
            var table = $('#myTable').DataTable();
         
            $('#myTable tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
 
        } );
        
        // $(document).ready(function () {
        //     $('#sidebarCollapse').on('click', function () {
        //         $('#sidebar').toggleClass('active');
        //         $(this).toggleClass('active');
        //         $('.title').css("display", "");
        //     });
        // });
        
        
   
    </script>
</body>

</html>


