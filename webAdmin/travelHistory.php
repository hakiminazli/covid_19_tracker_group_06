<?php
    
    include 'dbConnection.php';
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Travel History</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    
    <!-- Our Custom CSS -->
    <!--<link rel="stylesheet" href="travelHistory.css">-->

    <!-- Font Awesome JS -->
    <script src="https://kit.fontawesome.com/43d73fdaf8.js" crossorigin="anonymous"></script>
    
    
    <style>
        /* Style inputs, select elements and textareas */
        
        .container {
            padding: 100px 100px 100px 100px;
            align-items: center;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            position:relative;
            margin-top: 30px;
            color: black;
            background-color: rgba(12, 7, 21, 0.25);
            padding: 30px;
            border-radius: 15px;
            
        }
          
        .table-row{
            cursor:pointer;
        }

          /* Style the label to display next to the inputs */
         
          
          /* Style the submit button */
          
        
          
          /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
          /*@media screen and (max-width: 600px) {*/
          /*  .col-25, .col-75, input[type=submit],input[type=reset]{*/
          /*    width: 100%;*/
          /*    margin-top: 2px;*/
          /*    margin-right: 0px;*/
          /*  }*/
          /*} */
        
        
        
        /*footer {*/
        /*    padding-top: 10px;*/
        /*    padding-bottom: 20px;*/
        /*    background-color:white;*/
        /*    position:absolute;*/
        /*    bottom: 10px;*/
        /*    height: 2.5rem;  */
        /*}*/
          
        /*footer p {*/
        /*    margin-bottom: .25rem;*/
        /*}*/
        
       .sidebar-header img{
            width: 70px;   
            display:block;
            margin: 30px auto;
            padding: auto;
        
        }
        
                /*
            DEMO STYLE
        */
        @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";
        
        
        body {
            font-family: 'Poppins', sans-serif;
            /*background: #fafafa;*/
            /*background-color: #00DBDE;*/
            /*background-image: linear-gradient(90deg, #00DBDE 0%, #FC00FF 100%);*/
            background-color: #74EBD5;
            background-image: linear-gradient(90deg, #74EBD5 0%, #9FACE6 100%);

        }
        
        #myTable thead{
            background-color:rgba(12, 7, 21, 0.9);
            color: white;
        }
        
        /*#myTable thead:hover{*/
        /*    background-color:rgba(12, 7, 21, 0.9);*/
        /*}*/
        
        /*#myTable{*/
        /*    border: 1px solid rgba(12, 7, 21, 0.9);*/
        }
        
        td{
            background-color: black;
        }
        
        tbody:hover{
            background-color: white;
        }
        
        thead:hover{
         background-color: black;
        }
        
        .info{
            padding: 20px;
            text-align: center;
        }
        
        p {
            font-family: 'Poppins', sans-serif;
            font-size: 1.1em;
            font-weight: 300;
            line-height: 1.7em;
            color: #999;
        }
        
        a, a:hover, a:focus {
            color: inherit;
            text-decoration: none;
            transition: all 0.3s;
        }
        
        .navbar {
            background-color: rgba(12, 7, 21, 0.9);
            padding: 15px 10px;
            border: none;
            border-radius: 1%;
            margin-bottom: 30px;
            box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.19);
        }
        
        .navbar-btn {
            box-shadow: none;
            outline: none !important;
            border: none;
        }
        
        .line {
            width: 100%;
            height: 1px;
            border-bottom: 1px dashed #ddd;
            margin: 40px 0;
        }
        
        /* ---------------------------------------------------
            SIDEBAR STYLE
        ----------------------------------------------------- */
        
        /*.wrapper{*/
        /*    display: flex;*/
        /*    width: 100%;*/
        /*    align-items: stretch;*/
        /*    perspective: 1500px;*/
        /*}*/
        
        
         #sidebar {
            min-width: 250px;
            max-width: 250px;
            background-color: rgba(12, 7, 21, 0.9);
            color: #fff;
            transition: all 0.6s cubic-bezier(0.945, 0.020, 0.270, 0.665);
            transform-origin: bottom left;
            position:fixed;
            height: 100%;
            border-right: 7px solid #00DBDE;
            /*overflow-y: scroll;*/
             
        }
        
        /*#sidebar.active {*/
        /*    margin-left: -250px;*/
        /*    transform: rotateY(100deg);*/
        /*}*/
        
        #sidebar .sidebar-header {
            padding: 20px;
            background: rgba(0,0,1,0);
            border-radius: 15px;
        }
        
        #sidebar ul.components {
            padding: 20px 0;
            border-top: 1px solid #62c1cd;*/
        }
        
        #sidebar ul p {
            color: rgba(0,0,1,0.3);
            padding: 10px;
        }
        
        #sidebar ul li a {
            padding: 10px;
            font-size: 1.1em;
            display: block;
        }
        #sidebar ul li a:hover {
            color: #62c1cd;
            background: #fff;
        }
        
        #sidebar ul li.active > a, a[aria-expanded="true"] {
            color: #fff;
            background: #00DBDE;
        }
        
        
        a[data-toggle="collapse"] {
            position: relative;
        }
        
        .dropdown-toggle::after {
            display: block;
            position: absolute;
            top: 50%;
            right: 20px;
            transform: translateY(-50%);
        }
        
        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: rgb(43, 44, 49);
        }
        
        ul.CTAs {
            padding: 20px;
        }
        
        ul.CTAs a {
            text-align: center;
            font-size: 0.9em !important;
            display: block;
            border-radius: 5px;
            margin-bottom: 5px;
        }
        
        a.logout {
            margin-top:100px;
            background: #00DBDE;
            color: #fff;
        }
        
        a.article, a.article:hover {
            background: rgb(43, 44, 49) !important;
            color: #fff !important;
        }
        
        
        
        /* ---------------------------------------------------
            CONTENT STYLE
        ----------------------------------------------------- */
        #content {
            width: 81%;
            padding: 20px;
            min-height: 100vh;
            transition: all 0.3s;
            margin-left: 19%;
        }
        
        /*#sidebarCollapse {*/
        /*    width: 40px;*/
        /*    height: 40px;*/
        /*    background:rgb(43, 44, 49);*/
        /*    cursor: pointer;*/
        /*}*/
        
        /*#sidebarCollapse span {*/
        /*    width: 80%;*/
        /*    height: 2px;*/
        /*    margin: 0 auto;*/
        /*    display: block;*/
        /*    background: #fff;*/
        /*    transition: all 0.8s cubic-bezier(0.810, -0.330, 0.345, 1.375);*/
        /*    transition-delay: 0.2s;*/
        /*}*/
        
        /*#sidebarCollapse span:first-of-type {*/
        /*    transform: rotate(45deg) translate(2px, 2px);*/
        /*}*/
        /*#sidebarCollapse span:nth-of-type(2) {*/
        /*    opacity: 0;*/
        /*}*/
        /*#sidebarCollapse span:last-of-type {*/
        /*    transform: rotate(-45deg) translate(1px, -1px);*/
        /*}*/
        
        
        /*#sidebarCollapse.active span {*/
        /*    transform: none;*/
        /*    opacity: 1;*/
        /*    margin: 5px auto;*/
        /*}*/
        
        
        /* ---------------------------------------------------
            MEDIAQUERIES
        ----------------------------------------------------- */
        @media (max-width: 899px) {
            #sidebar {
                margin-left: -250px;
                transform: rotateY(90deg);
                position:fixed;
                 z-index: 1;
            }
            
            #content {
                width: 100%;
                /*padding: 20px;*/
                /*min-height: 100vh;*/
                /*transition: all 0.3s;*/
                margin-left: auto;
            }
            /*#sidebar.active {*/
            /*    margin-left: 0;*/
            /*    transform: none;*/
            /*}*/
            /*#sidebarCollapse span:first-of-type,*/
            /*#sidebarCollapse span:nth-of-type(2),*/
            /*#sidebarCollapse span:last-of-type {*/
            /*    transform: none;*/
            /*    opacity: 1;*/
            /*    margin: 5px auto;*/
            /*}*/
            /*#sidebarCollapse.active span {*/
            /*    margin: 0 auto;*/
            /*}*/
            /*#sidebarCollapse.active span:first-of-type {*/
            /*    transform: rotate(45deg) translate(2px, 2px);*/
            /*}*/
            /*#sidebarCollapse.active span:nth-of-type(2) {*/
            /*    opacity: 0;*/
            /*}*/
            /*#sidebarCollapse.active span:last-of-type {*/
            /*    transform: rotate(-45deg) translate(1px, -1px);*/
            /*}*/
        
        }



    </style>


</head>

<body>

    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <a href='homePage.php'><img src="assets/TRACKER.png" alt="Coivd-19 Tracker"></a>
            </div>

            <ul class="list-unstyled components">
                <li>
                    <a href="homePage.php"><i class="fas fa-tachometer-alt"></i>  Dashboard</a>
               <li>
                    <a href="uploadNewsPage.php"><i class="fas fa-newspaper"></i>  Upload News</a>
                </li>
                 <li  class="active">
                    <a href="travelHistory.php"><i class="fas fa-route"></i>  Travel History</a>
                </li>
                 <li>
                    <a href="redZone.php"><i class="fas fa-head-side-mask"></i>  Red Zones</a>
                </li>
                <!--<li>-->
                <!--    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Actions</a>-->
                <!--    <ul class="collapse list-unstyled" id="pageSubmenu">-->
                <!--        <li class="active">-->
                <!--            <a href="uploadNewsPage.php">Upload News</a>-->
                <!--        </li>-->
                <!--        <li>-->
                <!--            <a href="travelHistory.php">User Travel History</a>-->
                <!--        </li>-->
                <!--        <li>-->
                <!--            <a href="#">Red Flagge Areas</a>-->
                <!--        </li>-->
                <!--    </ul>-->
                <!--</li>-->
                <!--<li>-->
                <!--    <a href="#">Profile</a>-->
                <!--</li>-->
                <li>
                    <a href="help.php"><i class="fas fa-question-circle"></i>  Help</a>
                    
                </li>
                <!--<li>-->
                    <!--<a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>-->
                <!--    <a class="logout" href="logoutAdmin.php"><i class="fas fa-sign-out-alt"></i> Sign Out </a>-->
                <!--</li>-->
            </ul>

            <!--<ul class="list-unstyled CTAs">-->
            <!--    <li>-->
                    <!--<a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>-->
            <!--        <a class="logout" href="logoutAdmin.php">Sign Out <i class="fas fa-sign-out-alt"></i></a>-->
            <!--    </li>-->
            <!--</ul>-->
        </nav>
     
        <!-- Page Content Holder -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-dark bg">
                <div class="container-fluid">

                    <!--<button type="button" id="sidebarCollapse" class="navbar-btn">-->
                    <!--    <span></span>-->
                    <!--    <span></span>-->
                    <!--    <span></span>-->
                    <!--</button>-->
                    <!--<button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">-->
                    <!--    <i class="fas fa-align-justify"></i>-->
                    <!--</button>-->
                    <div class="title">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link"  href="#"><h4 style="margin:0;">Travel History</h4></a>
                            </li>
                        </ul>
                    </div>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="homePage.php">Dashboard</a>
                            </li>
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            Dropdown link
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Link 1</a>
                                <a class="dropdown-item" href="#">Link 2</a>
                                <a class="dropdown-item" href="#">Link 3</a>
                            </div>
                        </li>
                            <li class="nav-item">
                                 <a class="nav-link" data-toggle="tooltip" title="Sign Out"href="logoutAdmin.php"><i class="fas fa-sign-out-alt"></i></a>
                            </li>
                         
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <h1>List of Users</h1>
            <!--    <label for="state">State: </label>-->
            <!--    <select  id="state" name="states">-->
            <!--        <option value="all">All</option>-->
            <!--        <option value="perak">Perak</option>-->
            <!--        <option value="selangor">Selangor</option>-->
            <!--        <option value="pulau pinang">Pulau Pinang</option>-->
            <!--        <option value="negeri sembilans">Negeri Sembilan</option>-->
            <!--        <option value="melaka">Melaka</option>-->
            <!--        <option value="johor">Johor</option>-->
            <!--        <option value="pahang">Pahang</option>-->
            <!--        <option value="terengganu">Terengganu</option>-->
            <!--        <option value="kelantan">Kelantan</option>-->
            <!--        <option value="kedah">Kedah</option>-->
            <!--        <option value="perlis">Perlis</option>-->
            <!--    </select> -->
        
            <!--    <label for="status">Status: </label>      -->
            <!--    <select  id="status" name="status">-->
            <!--        <option value="All">All</option>-->
            <!--        <option value="normal">Normal</option>-->
            <!--        <option value="danger">Danger</option>-->
            
            <!--    </select> -->
                  
            <!--    <label for="gender">Gender: </label>      -->
            <!--    <select  id="gender" name="status">-->
            <!--        <option value="All">All</option>-->
            <!--        <option value="male">Male</option>-->
            <!--        <option value="female">Female</option>-->
            <!--    </select> -->
            
                <table id="myTable" class='table table-bordered table-dark table-hover'>
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>IC</th>
                 
                    <th>Status</th>
                </thead>
                <tbody>
                </tr>
            
                        
                     <?php
                
                
                        $sql= "SELECT IC, name, gender, zoneStatus FROM user_info";
                        $result = $con->query($sql);
                        
                        if($result->num_rows > 0){
                           while($row = $result->fetch_assoc()){
                               if(strtolower($row["zoneStatus"]) == "danger"){
                                   echo "<tr style= 'background-color: rgb(247, 57, 57);color:white;' class='table-row'>";
                                   echo "<td>".$row["name"]."</td>";
                                   echo "<td class='userIC'>".$row["IC"]."</td>";
                                   echo "<td>".$row["zoneStatus"]."</td>";
                                   echo "</tr>";
                               }else{
                                   echo "<tr style= 'background-color: rgb(23, 43, 42);color:white;' class='table-row'>";
                                   echo "<td>".$row["name"]."</td>";
                                   echo "<td class='userIC'>".$row["IC"]."</td>";
                                   echo "<td>".$row["zoneStatus"]."</td>";
                                   echo "</tr>";
                               }
                               
                            //   echo "<td>".$row["name"]."</td>";
                            //   echo "<td class='userIC'>".$row["IC"]."</td>";
                            //   echo "<td>".$row["zoneStatus"]."</td>";
                            //   echo "</tr>";
                               
                           }
                            
                        }else{
                            echo "error";
                        }
                        
                   
                        $con->close();
                    ?>
                
                </tbody>
                    
                </table> 
        
        
            </div>
      
        </div>
    </div>
    <!-- /#page-content-wrapper -->



  
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
    
  
    <script type="text/javascript">
     
        $(document).ready(function() {
            var table = $('#myTable').DataTable();
         
            $('#myTable tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
 
        } );
        
        
        $(document).ready(function($) {
            $(".table-row").click(function() {
                var currentRow=$(this).closest("tr"); 
                var zone = currentRow.find("td:eq(3)").text();
                
                console.log(zone);
                
                if(zone == "danger"){
                    $(this).children('td, th').css('background-color','red');
                }
                
                var ic = currentRow.find("td:eq(1)").text(); // get current row 1st TD value
                var enic = encodeURI(ic);
                window.location.href = "userTravelDetails.php?ic="+enic;
                
                // $.ajax({
                //   type: "POST",
                //   url: "travelHistory.php",
                //   data: {IC:ic},
                //   var enic = encodeURI(ic);
                  
                //   success: function(msg){
                //     setTimeout(function() {
                //       location.href = "userTravelDetails.php?lat="+enic+";
                //     });
                //   },
                //   error: function(XMLHttpRequest, textStatus, errorThrown) {
                //      alert("some error");
                //   }
                // });
            });
        });
    </script>
</body>

</html>


